#!/bin/bash
# don't change these
red='\E[31m'
green='\E[32m'
blue='\E[37m'
#
# This script will test cop4530 proj1 by running a series of tests on the maze
# files provided in the proj folder.
#
# It create a hidden director (.testing) in the hw1 folder to keep the output
# of each run and the diff results. If you want to manually review the 
# results you can look in the folder.
#
TEST_DIR=~/cop4530/proj1/.testing       # Location output and diff files
HISPATH=~/cop4530/proj1                 # Location of area51/ratpack_i.x mazetest_i.x
MYPATH=~/cop4530/proj1                  # Location of your ratpack.x mazetest.x
MAZEFILEPATH=~/cop4530/proj1/.mazes     # Location of your maze files
WAIT=3									# Number in seconds for how long you want output to pause
#
# Feel free to change this color to something that looks better on your screen
# Leave it blank to use your default. Choices are { $red, $green, $blue, "" } you can add others if you want
export color=$red
#
# Function to reset screen color to default
colorReset() 
{
  tput sgr0
}
#
# Testing if maze directory exists, copy it over if not. If that fails, exit.
if [ ! -d $MAZEFILEPATH ] ; then      
  echo -ne "\n$MAZEFILEPATH does not exist.\n\tCreating $MAZEFILEPATH and copying mazes into it ... "
  if cp -r /home/majors/jscott/public/mazes .mazes ; then
    echo -e "Success"
  else
    echo -e "Failed"
    echo -e "Edit the \$MAZEFILEPATH variable in the script and try again"
    exit 1
  fi
fi
#
# Get Maze Files
GOODMAZEFILES=$(find $MAZEFILEPATH/ ! -iname \*bad\* -type f -exec basename {} \;)    # String 
BADMAZEFILES=$(find $MAZEFILEPATH/ -iname \*bad\* -type f -exec basename {} \;)    # String 
#
# Test if either set of MAZE files are empty strings
if [ -z "$GOODMAZEFILES" ] ; then
  echo -e "\n$GOODMAZEFILES is an empty String.\n\tMake sure you've specified the correct path for \$MAZEFILEPATH in the script and check to make sure the directory isn't empty.\n"
  exit 1
fi
if [ -z "$BADMAZEFILES" ] ; then
  echo -e "\n$BADMAZEFILES is an empty String.\n\tMake sure you've specified the correct path for \$MAZEFILEPATH in the script and check to make sure the directory isn't empty.\n"
  exit 1
fi
#
# Pause
function pause()
{
  echo -en $color
  echo -e "\n\tPress Enter to Continue ..."
  tput sgr0
  read
}
#
# If the testing directory doesn't exist, create it first.
if [ ! -d $TEST_DIR ] ; then      
  mkdir $TEST_DIR
fi
#
# Test to see if executables are reachable
if [ ! -f $HISPATH/mazetest_i.x ] ; then
  echo -en "\nFailed to find: $HISPATH/mazetest_i.x\n\tFetching them from area51 ... "
  if cp ~cop4530p/spring14/area51/mazetest_i.x $HISPATH/mazetest_i.x ; then
    echo -e "Success"
    chmod u+x $HISPATH/mazetest_i.x
  else
    echo -e "Failed!\n Fix the variable \$HISPATH in the script and try again"
    exit 1
  fi
fi
#
# Test for mymaze file
if [ ! -f $MAZEFILEPATH/mymaze ] ; then
  echo -e "\nFailed to find: $MAZEFILEPATH/mymaze\n\tYou may need to copy your mymaze file into: $MAZEFILEPATH \n"
  exit 1
fi
# If the make fails, report failure and exit script.
if ! make >> /dev/null 2>&1 ; then
  echo -e "make failed:\n\tRun make manually to find problem and try again.\n::Exiting Script::"
  exit 1
fi
#
# Test to see if your executable is reachable
if [ ! -f $MYPATH/mazetest.x ] ; then
  echo -e "\nFailed to find: $MYTH/mazetest.x\n\tCheck this path and try again.\n"
  exit 1
fi
pushd $MAZEFILEPATH
#
# Now we run through a series of tests with known bad results
#
# Begin Test 1
clear
echo -en $color
echo -e "\n\tTest 1\n =====================================================================================" 
echo -e "\nThis is to test if you can run Initialize() more than once without problem."
echo -e "If this causes a seg fault you need to look at your Initialize() function"
echo -e "\n =====================================================================================" 
colorReset
echo -e "1\nmymaze\n2\n1\nmymaze\n1\nmaze0\n1\nmymaze\n0\n0" | $MYPATH/mazetest.x |tee $TEST_DIR/mymaze.mazetest.test1 2>&1
echo -e  "You can find the output at: $TEST_DIR/mymaze.mazetest.test1"
pause
# End Test 1
# Begin Test 2
clear
echo -en $color
echo -e "\n\tTest 2\n =====================================================================================" 
echo -e "\nThis is to test if you can run Initialize() with more than one file. It will open mymaze"
echo -e "and then maze0 and then mymaze again"
echo -e "If this causes a seg fault you need to look at your Initialize() function"
echo -e "\n =====================================================================================" 
colorReset
echo -e "1\nmymaze\n2\n1\nmymaze\n1\nmaze0\n1\nmymaze\n0\n0" | $MYPATH/mazetest.x |tee $TEST_DIR/mymaze.mazetest.test2 2>&1
echo -e  "You can find the output at: $TEST_DIR/mymaze.mazetest.test2"
pause
# End Test 2
# Begin Test 3
clear
echo -en $color
echo -e "\n\tTest 3\n =====================================================================================" 
echo -e "\nThis is to test if you can run Consistent() more than once without problem."
echo -e "If this causes a seg fault you need to look at your Consistent() function"
echo -e "\n =====================================================================================" 
colorReset
echo -e "1\nmymaze\n2\n2\n\n0\n0" | $MYPATH/mazetest.x |tee $TEST_DIR/mymaze.mazetest.test3 2>&1
echo -e  "You can find the output at: $TEST_DIR/mymaze.mazetest.test3"
pause
# End Test 3
# Begin Test 4
clear
echo -en $color
echo -e "\n\tTest 4\n =====================================================================================" 
echo -e "\nThis is to test if you can run Consistent() with more than one file without problem."
echo -e "If this causes a seg fault you need to look at your Consistent() function"
echo -e "\n =====================================================================================" 
colorReset
echo -e "1\nmymaze\n2\n1\nmaze3\n2\n1\nmaze.big\n2\n\n0\n0" | $MYPATH/mazetest.x |tee $TEST_DIR/mymaze.mazetest.test4 2>&1
echo -e  "You can find the output at: $TEST_DIR/mymaze.mazetest.test4"
pause
# End Test 4
# Begin Test 5
clear
echo -en $color
echo -e "\n\tTest 5\n =====================================================================================" 
echo -e "\nThis is to test if you can run Solve() more than once without problem."
echo -e "If this causes a seg fault you need to look at your Solve() function"
echo -e "You should also be on the lookout for your results growing each time Solve() is called."
echo -e "\n =====================================================================================" 
colorReset
echo -e "1\nmymaze\n2\n3\n3\n3\n3\n3\n0\n0" | $MYPATH/mazetest.x |tee $TEST_DIR/mymaze.mazetest.test5 2>&1
echo -e  "You can find the output at: $TEST_DIR/mymaze.mazetest.test5"
pause
# End Test 5
# Begin Test 6
clear
echo -en $color
echo -e "\n\tTest 6\n =====================================================================================" 
echo -e "\nI discovered this one while writing this script. maze4 can't be solved, and mine was reporting"
echo -e "a solution for it. If this happens to you, run some manual tests and see why."
echo -e "\n =====================================================================================" 
colorReset
echo -e "1\nmaze4\n2\n3\n4\n0\n0" | $MYPATH/mazetest.x |tee $TEST_DIR/mymaze.mazetest.test6 2>&1
echo -e  "You can find the output at: $TEST_DIR/mymaze.mazetest.test6"
pause
# End Test 6
# Begin Test 7
clear
echo -en $color
echo -e "\n\tTest 7\n =====================================================================================" 
echo -e "\nThis is another one I discovered this one while writing this script. maze0 has a more than one"
echo -e "solution with the same number of steps. Don't be surprise if your solution is different that the"
echo -e "one provided in area51\n"
echo -e "\n =====================================================================================" 
colorReset
echo -e "1\nmaze0\n2\n3\n4\n0\n0" | $MYPATH/mazetest.x |tee $TEST_DIR/mymaze.mazetest.test7 2>&1
echo -e  "You can find the output at: $TEST_DIR/mymaze.mazetest.test7"
pause
# End Test 7

#
# Test Solutions to Good Files
#
clear
echo -en $color
echo -e "\n\tGood Maze Files\n =====================================================================================" 
echo -e "This next series of tests will test good files. If there is nothing different between your output"
echo -e "and area51 then you will see it scroll by fast. If there is a difference you will have a $WAIT sec pause."
echo -e "\nThis may not work for you as well as for me. I go to great lengths to make sure my output matches"
echo -e "the area51 provided binaries. It makes my automated testing easier."
echo -e "\nIf you are having problems with it, try reviewing the diff files manually afterward."
echo -e "\n =====================================================================================" 
colorReset
pause
FILES=$GOODMAZEFILES
MOREINPUT="\n2\n3\n"            # Run 1, 2 
#MOREINPUT="\n2\n3\n4\n3\n3\n4" # Run 1, 2, 3, 4, 3, 3, 4 
#MOREINPUT="\n2\n4"
for file in $FILES ; do
  input="1\n$file\n$MOREINPUT\n1\nmaze0\n$MOREINPUT\n0\n0"
  echo -e $input | $HISPATH/mazetest_i.x > $TEST_DIR/his.$file.mazetest 2>&1
  echo -e $input | $MYPATH/mazetest.x > $TEST_DIR/mine.$file.mazetest 2>&1
  diff $TEST_DIR/his.$file.mazetest $TEST_DIR/mine.$file.mazetest > $TEST_DIR/diff.$file.mazetest
  diffstatus=$?
  echo -en $color
  echo -e "\n\t BEGIN $file \n =====================================================================================" 
  colorReset
  cat $TEST_DIR/diff.$file.mazetest
  echo -en $color
  echo -e "\n\t END $file \n =====================================================================================" 
  colorReset
  if (( $diffstatus )) ; then sleep $WAIT ; fi 
done
pause
#
# Test Solutions to Bad Files
#
clear
echo -en $color
echo -e "\n\tBad Maze Files\n =====================================================================================" 
echo -e "This next series of tests will test bad maze files. If there is nothing different between your output"
echo -e "and area51 then you will see it scroll by fast. If there is a difference you will have a $WAIT sec pause.\n"
echo -e "\nWhen I first started testing with the bad files, I was seeing Seg Faults all over the place. I was able"
echo -e "to isolate all of the issues by manually testing with the mazefiles that were causing seg faults."
echo -e "\n =====================================================================================" 
colorReset
pause
FILES=$BADMAZEFILES
MOREINPUT="\n2\n3\n"            # Run 1, 2 
#MOREINPUT="\n2\n3\n4\n3\n3\n4" # Run 1, 2, 3, 4, 3, 3, 4 
#MOREINPUT="\n2\n4"
for file in $FILES ; do
  input="1\n$file\n$MOREINPUT\n1\nmaze0\n$MOREINPUT\n0\n0"
  echo -e $input | $HISPATH/mazetest_i.x > $TEST_DIR/his.$file.mazetest 2>&1
  echo -e $input | $MYPATH/mazetest.x > $TEST_DIR/mine.$file.mazetest 2>&1
  diff $TEST_DIR/his.$file.mazetest $TEST_DIR/mine.$file.mazetest > $TEST_DIR/diff.$file.mazetest
  diffstatus=$?
  echo -en $color
  echo -e "\n\t BEGIN $file \n =====================================================================================" 
  colorReset
  cat $TEST_DIR/diff.$file.mazetest
  echo -en $color
  echo -e "\n\t END $file \n =====================================================================================" 
  colorReset
  if (( $diffstatus )) ; then sleep $WAIT ; fi 
done
popd

